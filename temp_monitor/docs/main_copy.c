/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */

GPIO_InitTypeDef  ONE_Wire_Out;
GPIO_InitTypeDef  ONE_Wire_In;
GPIO_InitTypeDef  LED_Out;
/*
 *   while (1)
  {
	  while(!(TIM2->SR & TIM_SR_UIF)){

	  }
		TIM2->SR = 0;
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, 0);//~HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_10)

		while(!(TIM2->SR & TIM_SR_UIF)){

		}

		TIM2->SR = 0;
	  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, 1);
  }
 */
void start_timer(){
	__TIM2_CLK_ENABLE();
	/*TIM_Base_InitTypeDef TIM2_base_conf = {
			.Prescaler = 72,
			.CounterMode = TIM_COUNTERMODE_UP,
			.Period = 65535-1000,
			.ClockDivision = TIM_CLOCKPRESCALER_DIV1,
			.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE
	};*/

	TIM_HandleTypeDef htim = {
			.Instance = TIM2,
			.Init = {
					.Prescaler = 72,
					.CounterMode = TIM_COUNTERMODE_UP,
					.Period = 65535-1000,
					.ClockDivision = TIM_CLOCKPRESCALER_DIV1,
					.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE
			},
			.Channel = HAL_TIM_ACTIVE_CHANNEL_1
	};
	HAL_TIM_Base_Init(&htim);
	__HAL_RCC_TIM2_CLK_ENABLE();
	//HAL_TIM_Base_Init(&TIM2_conf);
	HAL_TIM_Base_Start(&htim);
	//TIM2_conf.Instance = TIM2;
	/*
	TIM2_conf.Prescaler = 72;
	TIM2_conf.CounterMode = TIM_COUNTERMODE_UP;
	TIM2_conf.Period = 65535-30;
	TIM2_conf.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	TIM2_conf.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	*/


	/*
	HAL_TIM_Base_MspInit(HAL_TIM_Base_Init);
	__HAL_RCC_TIMx_CLK_ENABLE()

	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	TIM2->SR = 0;
	TIM2->PSC = 0;

	TIM2->ARR = 0xf12;
	TIM2->CR1 |=  TIM_CR1_CEN;
	COMIE |= TIM2_DIER;
	//TIM2->CR1 |=  1;*/
}


void delay_us(uint32_t us) {
	uint32_t counter = 10*(us-1);
	while(counter){
		counter -= 1;
	}
}

uint32_t count_us(uint32_t max_wait){
	HAL_GPIO_Init(GPIOA, &ONE_Wire_In);
	max_wait *= 10;
	uint32_t counter = 0;
	while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_8) == 1 && max_wait != 0) {
		max_wait -= 1;
	}
	if(max_wait == 0){
		return 0;
	}
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 1);
	while(HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_8) == 0) counter++;
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 0);
	return counter/10;
}

uint8_t read_bit(){
	uint32_t zero_us = count_us(10);
}

void LH_signal(uint32_t L_time, uint32_t H_time) {
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 0);
	delay_us(L_time);//From pullup_HIGH to GND_LOW:---___
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 1);
	delay_us(H_time);//From GND_LOW to pullup_HIGH:___---
}

uint32_t reset(){
	LH_signal(500, 16);
	return count_us(200);
}


void write_bit(uint8_t bit){
	if(bit == 0) LH_signal(64, 5);
	else LH_signal(6, 22);
}

void write_byte(uint8_t data){
	HAL_GPIO_Init(GPIOA, &ONE_Wire_Out);
	// HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 1);
	for(uint8_t i = 0; i < 8; i++){
		write_bit(data >> i & 0b1);
	}
}

int main(void)
{
	TIM_OnePulse_InitTypeDef Tim_Gen_Init;
	Tim_Gen_Init.OCMode = TIM_OPMODE_SINGLE;
	Tim_Gen_Init.Pulse = 20;
	Tim_Gen_Init.OCPolarity = TIM_OCPOLARITY_LOW;
	Tim_Gen_Init.OCIdleState = TIM_OCIDLESTATE_SET;
	Tim_Gen_Init.ICPolarity = TIM_ICPOLARITY_RISING;
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
  ONE_Wire_Out.Pin = GPIO_PIN_8;
  ONE_Wire_Out.Mode = GPIO_MODE_OUTPUT_OD;
  ONE_Wire_Out.Pull = GPIO_NOPULL;
  ONE_Wire_Out.Speed = GPIO_SPEED_HIGH;

  ONE_Wire_In.Pin = GPIO_PIN_8;
  ONE_Wire_In.Mode = GPIO_MODE_INPUT;
  ONE_Wire_In.Pull = GPIO_NOPULL;
  ONE_Wire_In.Speed = GPIO_SPEED_HIGH;

  LED_Out.Pin = GPIO_PIN_10;
  LED_Out.Mode = GPIO_MODE_OUTPUT_PP;
  LED_Out.Pull = GPIO_NOPULL;
  LED_Out.Speed = GPIO_SPEED_HIGH;

  HAL_GPIO_Init(GPIOB, &LED_Out);
  //HAL_GPIO_Init(GPIOA, &ONE_Wire_Out);
  HAL_GPIO_Init(GPIOA, &ONE_Wire_In);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  uint32_t us = 0;
  uint32_t bkpt = 0;
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, 0);
  uint32_t dataRx [9*8];
  while (1)
  {
	  //write_bit(0);
	  //HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, 0);
	  //delay_us(1000);
	  //HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, 1);
	  //delay_us(5600000);
	  us = reset();
	  write_byte(0b11110000);
	  bkpt=bkpt+1;
	  /*
	   if(us > 7){
		  // sensor is present
		  // skip ROM command
		  write_byte(0xCC);

		  // issue write scratchpad command
		  write_byte(0x4E);

		  // send Th
		  write_byte(0xFF);
		  // send Tl
		  write_byte(0xFF);
		  // send Tconfig
		  write_byte(0b11);



		  reset();
		  // skip ROM command
		  write_byte(0xCC);
		  // issue read scratchpad command
		  write_byte(0xBE);

		  for(uint32_t i = 0; i < 9*8; i++){
			  dataRx[i] = read_bit();
		  }
		  bkpt=bkpt+1;

	  }
	  else{
		  bkpt=bkpt+1;
	  }*/
	  //HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 1);
	  //HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, 0);

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 288;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {

  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
