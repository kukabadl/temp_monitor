#pragma once
#include "queue.h"

typedef enum ONE_Wire_State {STOPPED, SENDING, RECEIVING, DONE_RECEIVING, DONE_SENDING, FAILED_RECEIVING, FAILED_SENDING} ONE_Wire_State;

typedef struct BIT {
	uint16_t LOW_period_us;
	uint16_t HIGH_period_us;
} BIT;

BIT pDefBit[2] = {
	//Default ZERO timings
	{
		.LOW_period_us = 65,
		.HIGH_period_us = 15
	},

	//Default ONE timings
	{
		.LOW_period_us = 7,
		.HIGH_period_us = 45
	}
};

typedef struct onewire{
	uint16_t Pin;
	GPIO_TypeDef* Port;
	GPIO_InitTypeDef Pin_Out;
	GPIO_InitTypeDef Pin_In;
	TIM_HandleTypeDef* pHtim;

	BIT* DefBit;
	ONE_Wire_State status;
	uint16_t sending;

	uint8_t Byte_Being_Sent;
	uint8_t idx_Of_Current_Bit;
	uint8_t bit_sent;

	queue rx_data;
	queue tx_data;
} ONE_Wire;

void ONE_Wire_Send_Next(ONE_Wire* pONE_Wire, BIT* pBitDict){
	uint16_t delay;
	BIT* pBitTimings;
	if(pONE_Wire->idx_Of_Current_Bit < 16){
		uint8_t val = (pONE_Wire->Byte_Being_Sent >> pONE_Wire->idx_Of_Current_Bit/2) & 0b1;

		uint8_t mod = pONE_Wire->idx_Of_Current_Bit % 2;
		HAL_GPIO_WritePin(pONE_Wire->Port, pONE_Wire->Pin, mod);

		if(pBitDict == NULL){
			pBitTimings = &(pONE_Wire->DefBit[val]);
		}
		else pBitTimings = (pBitDict + 1);



		if(mod) delay = pBitTimings->HIGH_period_us;
		else delay = pBitTimings->LOW_period_us;

		TIM_TypeDef* pTim = pONE_Wire->pHtim->Instance;

		pTim->ARR = (uint32_t)delay;
		pTim->SR &= (uint32_t)(~TIM_SR_UIF);

		pONE_Wire->idx_Of_Current_Bit ++;
	}
	else{
		pONE_Wire->status = DONE_SENDING;
		HAL_GPIO_WritePin(pONE_Wire->Port, pONE_Wire->Pin, 1);
	}
}

void ONE_Wire_Send_Byte(ONE_Wire* pONE_Wire, uint8_t data){
	pONE_Wire->Byte_Being_Sent = data;
	pONE_Wire->idx_Of_Current_Bit = 0;
	pONE_Wire->bit_sent = 0;
	pONE_Wire->status = SENDING;
	pONE_Wire->pHtim->Instance->CNT = 0;
	ONE_Wire_Send_Next(pONE_Wire, NULL);
}

void ONE_Wire_RxByte(ONE_Wire* pONE_Wire){
	if(HAL_TIM_Base_Stop_IT(pONE_Wire->pHtim) != HAL_OK){
		Error_Handler();
	}
}


void ONE_Wire_Start(ONE_Wire* pONE_Wire){
	if(HAL_TIM_Base_Start_IT((pONE_Wire->pHtim)) != HAL_OK){
		Error_Handler();
	}
}

void ONE_Wire_Stop(ONE_Wire* pONE_Wire){
	if(HAL_TIM_Base_Stop_IT(pONE_Wire->pHtim) != HAL_OK){
		Error_Handler();
	}
}


void ONE_Wire_Pin_Init(ONE_Wire* pONE_Wire, GPIO_TypeDef* port, uint16_t pin, TIM_HandleTypeDef* htim){
	pONE_Wire->Port = port;
	pONE_Wire->Pin = pin;
	pONE_Wire->pHtim = (TIM_HandleTypeDef *)htim;

	pONE_Wire->Pin_Out.Pin = pin;
	pONE_Wire->Pin_Out.Mode = GPIO_MODE_OUTPUT_OD;
	pONE_Wire->Pin_Out.Pull = GPIO_NOPULL;
	pONE_Wire->Pin_Out.Speed = GPIO_SPEED_HIGH;

	pONE_Wire->idx_Of_Current_Bit = 0;
	pONE_Wire->DefBit = pDefBit;
	pONE_Wire->status = STOPPED;

	HAL_GPIO_Init(port, &(pONE_Wire->Pin_Out));
}




ONE_Wire one_wire;
