#include "stdint.h"
typedef struct que{
    uint16_t head;
    uint16_t tail;
    uint16_t len;
    uint16_t n_used;
    uint8_t* pData;
} queue;

uint8_t push(queue* q, uint16_t len, uint8_t* pData){
    // Ladovat data od tail, kontrolovat, jestli se nedostanu na head
    for (uint16_t i = 0; i < len; i++){
        if(q->n_used >= q->len){
            return (len-i);
        }
        q->pData[q->tail] = pData[i];
        q->n_used++;
        if(q->tail < q->len-1) q->tail++;
        else q->tail = 0;
    }
    return 0;
}

uint8_t pop(queue* q){
    // Vratit pismeno na pozici head, pokud ex a o 1 posunout head
	uint8_t c;
    if(q->n_used){
        c = q->pData[q->head];
        q->n_used--;

        if(q->head < q->len-1) q->head++;
        else q->head = 0;
        return c;
    }
    return 0;
}

uint8_t get(queue* q){
    // Vratit pismeno na pozici head, pokud ex.
    return 0;
}

void init_queue(queue* p_queue, uint8_t* pData, uint16_t len){
    p_queue->head = 0;
    p_queue->tail = 0;
    p_queue->len = len;
    p_queue->n_used = 0;
    p_queue->pData = pData;
}
